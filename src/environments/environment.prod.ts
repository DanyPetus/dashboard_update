export const environment = {
  production: true,
  url: 'https://moppa-system.uc.r.appspot.com',
  firebaseConfig: {
    apiKey: "AIzaSyBIcvqFdyFceWOdjNnejgBd19siDpTbjPo",
    authDomain: "moppa-system.firebaseapp.com",
    databaseURL: "https://moppa-system.firebaseio.com",
    projectId: "moppa-system",
    storageBucket: "moppa-system.appspot.com",
    messagingSenderId: "890358165682",
    appId: "1:890358165682:web:9204b941b2b41d702459ab",
    measurementId: "G-792DW2CQ9C"
  },
  endpoints: {
    login: '/login/',
    users: '/users/',
    services: '/services/',
    tariffsName: '/services/tariffs/names',
    tarrifs: '/services/tariffs/',
    documents: '/documents/'
  },
  extensions: {
    filter: 'filter',
    request: 'request',
    users: 'users', 
    services: 'services'
  }
};
