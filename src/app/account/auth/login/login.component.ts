import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';

// Libs
import { CookieService } from 'ngx-cookie-service';

// Services
import { AuthService } from 'src/app/core/services/auth/auth.service';

type UserFields = 'email' | 'password';
type FormErrors = {
  [u in UserFields]: string
};

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public newUser = false;
  public loginForm: FormGroup;
  public formErrors: FormErrors = {
    'email': '',
    'password': '',
  };
  public errorMessage: any;
  public loginButton = false;
  public errorGeneral = '';


  constructor(
    private fb: FormBuilder,
    private router: Router,
    private authService: AuthService,
    private cookieService: CookieService,
  ) {}

  ngOnInit(): void {
    this.initForm();
  }

  initForm() {
    this.loginForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });
  }

  login() {
    if (this.loginForm.valid) {
      this.loginButton = true;
      const userLogin = this.loginForm.value;
      this.handleLogin(userLogin)
    }
  }

  handleLogin({ email, password }) {
    this.authService.login(email, password).subscribe((resp: any) => {
        const { userInfo, userToken } = resp;
       
        if (userInfo.status === 1) {
          this.handleCookieAsigned(userInfo, userToken);
        } else {
          this.router.navigate(['./user/pending'])
        }
      },
      (err) => {
        this.loginButton = false;
        if (err.status === 500) {
          this.errorGeneral = 'Revisa tu email o usuario.';
        }
      }
    );
  }

  handleCookieAsigned({ uid,  roleType, firstName, secondName}, token) {

    this.cookieService.set('uid', uid);
    this.cookieService.set('name', `${firstName}  ${secondName}`);
    this.cookieService.set('role', roleType);
    this.cookieService.set('token', token);

    if (uid && roleType) {
      this.redirectUser(roleType)
    } else {
      this.cookieService.deleteAll();;
      this.router.navigate(['./login'])
    }
  };

  redirectUser(role:number) {
    switch (role) {
      case 1:
        this.router.navigate(['./admin']);
        break;
      default:
        break;
    }
  }

}