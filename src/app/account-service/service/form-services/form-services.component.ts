import { Component, OnInit, ViewChild, ElementRef, NgZone } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';

// Libs
import { CookieService } from 'ngx-cookie-service';
import Swal from 'sweetalert2'

// Services
import { UsersService } from 'src/app/core/services/users/users.service';
import { TechniciansServicesService } from 'src/app/core/services/technicians-services/technicians-services.service';

@Component({
  selector: 'app-form-services',
  templateUrl: './form-services.component.html',
  styleUrls: ['./form-services.component.scss']
})
export class FormServicesComponent implements OnInit {

  // Views
  MapView = true;
  formService = false;
  formRegister = false;

  // Map
  latitude: Number;
  longitude: Number;
  zoom = 17;

  styles: any[] = [{
    elementType: 'geometry',
    stylers: [{
      color: '#242f3e'
    }]
  },
  {
    elementType: 'labels.text.stroke',
    stylers: [{
      color: '#242f3e'
    }]
  },
  {
    elementType: 'labels.text.fill',
    stylers: [{
      color: '#746855'
    }]
  },
  {
    featureType: 'administrative.locality',
    elementType: 'labels.text.fill',
    stylers: [{
      color: '#d59563'
    }]
  },
  {
    featureType: 'poi',
    elementType: 'labels.text.fill',
    stylers: [{
      color: '#d59563'
    }]
  },
  {
    featureType: 'poi.park',
    elementType: 'geometry',
    stylers: [{
      color: '#263c3f'
    }]
  },
  {
    featureType: 'poi.park',
    elementType: 'labels.text.fill',
    stylers: [{
      color: '#6b9a76'
    }]
  },
  {
    featureType: 'road',
    elementType: 'geometry',
    stylers: [{
      color: '#38414e'
    }]
  },
  {
    featureType: 'road',
    elementType: 'geometry.stroke',
    stylers: [{
      color: '#212a37'
    }]
  },
  {
    featureType: 'road',
    elementType: 'labels.text.fill',
    stylers: [{
      color: '#9ca5b3'
    }]
  },
  {
    featureType: 'road.highway',
    elementType: 'geometry',
    stylers: [{
      color: '#746855'
    }]
  },
  {
    featureType: 'road.highway',
    elementType: 'geometry.stroke',
    stylers: [{
      color: '#1f2835'
    }]
  },
  {
    featureType: 'road.highway',
    elementType: 'labels.text.fill',
    stylers: [{
      color: '#f3d19c'
    }]
  },
  {
    featureType: 'transit',
    elementType: 'geometry',
    stylers: [{
      color: '#2f3948'
    }]
  },
  {
    featureType: 'transit.station',
    elementType: 'labels.text.fill',
    stylers: [{
      color: '#d59563'
    }]
  },
  {
    featureType: 'water',
    elementType: 'geometry',
    stylers: [{
      color: '#17263c'
    }]
  },
  {
    featureType: 'water',
    elementType: 'labels.text.fill',
    stylers: [{
      color: '#515c6d'
    }]
  },
  {
    featureType: 'water',
    elementType: 'labels.text.stroke',
    stylers: [{
      color: '#17263c'
    }]
  }
]


  @ViewChild('search')
  public searchElementRef: ElementRef;
  private geoCoder;
  address: string;

  // Marker
  latitudeService: Number;
  longitudeService: Number;

  // User
  uid: string;
  role: string;

  // Form Service
  public serviceForm: FormGroup;
  services: any;

  // Form Register
  public userForm: FormGroup;
  saveButton: boolean = false;
  gender: any;

  genders = [
    {
      id: 0,
      name: "Masculino",
      genderType: 1,
    },
    {
      id: 1,
      name: "Femenino",
      genderType: 2,
    },
    {
      id: 2,
      name: "Especificar",
      genderType: 3,
    }
  ];

  constructor(
    private fb: FormBuilder,
    private cookieService: CookieService,
    private usersService: UsersService,
    private techniciansServicesService: TechniciansServicesService,
  ) { 
    this.uid = this.cookieService.get('uid');
    this.role = this.cookieService.get('role');
  }

  ngOnInit(): void {
    this.setCurrentLocation();
  }

  private setCurrentLocation() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        
        this.latitudeService = position.coords.latitude;
        this.longitudeService = position.coords.longitude;
      });
    }
  }

  onMapReady(map) {
  }

  mapClicked(event) {
    this.latitudeService = event.coords.lat;
    this.longitudeService = event.coords.lng;
  }

  zoomIn() {
    this.zoom++;
  }

  zoomOut() {
    this.zoom--;
  }


  acceptServiceRequest() {
    Swal.fire({
      title: '¿Deseas solicitar el servicio en esta ubicación?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Aceptar',
      cancelButtonText: 'Cerrar'
    }).then((result) => {
      if (result.isConfirmed) {
        this.createServiceRequest();
      }
    })
  }

  createServiceRequest(){
    this.initFormService();
    this.getServices();
    this.MapView = false;
    this.formService = true;
  }

  initFormService() {
    this.serviceForm = this.fb.group({
      idService: [0, [Validators.required]],
      date: ['', [Validators.required]],
      hour: ['', [Validators.required]],
      commentary: ['', [Validators.required]],
    })
  }

  getServices(){
    const filter = {status: 1};
    this.techniciansServicesService.getServices(filter).subscribe((data:any) => {
      this.services = data;
    }, (err) => {
      console.log(err);
    });
  }

  createServiceRequestValidation(){
    if(!this.uid){
      this.createServiceRequestUser();
    }
  }
  
  createServiceRequestUser(){
    if(!this.uid){
      this.initForm();
      this.formService = false;
      this.formRegister = true;
    }else if(this.role === '3'){
    }else this.errorAlert('No cumple con los requisitos', 'Debes ser un cliente o no estar registrado para solicitar un servicio');
  }
  
  initForm() {
    this.userForm = this.fb.group({
      firstName: ['', [Validators.required]],
      secondName: ['', [Validators.required]],
      email: ['', [Validators.required]],
      phone: [0, [Validators.required]],
      // gender: ['', [Validators.required]],
      // birthdate: ['', [Validators.required]],
    })
  }

  saveGender(event: any){
    const index = Number(event.target.value);
    this.gender = this.genders[index];
  }

  saveUser() {
    if (this.userForm.valid) {
      this.handleCreateUser(this.userForm.value);
    }
  }

  handleCreateUser(user: any) {
    const userInfo = {
      user: {
        email: user.email,
        password: "12345678"
      },
      information: {
        firstName: user.firstName,
        secondName: user.secondName,
        phone: user.phone,
        birthdate: new Date(),
        gender: 'Sin asignar',
        genderType: 3,
        roleName: 'Cliente',
        roleType: 3,
        imageProfile: '',
        status: 1,
        typeRegister: 1,
        createBy: "PfNvSUQkqCSWd8Tmzxe2uwfRHhy2",
        updateBy: "PfNvSUQkqCSWd8Tmzxe2uwfRHhy2"
      },
    }

    this.usersService.createUser(userInfo).subscribe((resp: any) => {
        this.successAlert('Usuario Registrado', 'Usuario registrado y servicio programado con exito.');
      },
      (err) => {
        console.log(err);
      }
    );
  }

  successAlert(title: string, body: string) {
    Swal.fire(
      title,
      body,
      'success'
    ).then((resp => {
    }))
  }


  errorAlert(title: string, body: string) {
    Swal.fire(
      title,
      body,
      'error'
    ).then((resp => {
    }))
  }

}
