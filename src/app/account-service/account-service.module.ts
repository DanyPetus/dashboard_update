import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccountServiceRoutingModule } from './account-routing.module';
import { ServiceModule } from './service/service.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AccountServiceRoutingModule,
    ServiceModule
  ]
})
export class AccountServiceModule { }
