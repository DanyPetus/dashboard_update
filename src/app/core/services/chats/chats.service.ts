import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class ChatsService {

  chatsServices: AngularFirestoreCollection;

  constructor(
    private afStore: AngularFirestore,
  ) {
    this.chatsServices = this.afStore.collection('chats_services');
   }


  getChat(uid: string){
    const query = this.chatsServices.ref.doc(uid);
    return query.get();
  }

  updateChanges(uid: string){
    const query = this.chatsServices.ref.doc(uid);
    return query;
  }

  updateChat(uid:string, chat: any){
    return this.chatsServices.doc(uid).update(chat);
  }

}


