import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class TechniciansServicesService {

  token: string;
  httpOptions: any;

  constructor(
    private http: HttpClient,
    private cookieService: CookieService
  ) {
    this.token = this.cookieService.get('token');

    this.httpOptions = {
      headers: new HttpHeaders({
        'Authorization': this.token
      })
    };
  }

  getServices(filter) {
    return this.http.post(`${environment.url + environment.endpoints.services + environment.extensions.filter }`, filter, this.httpOptions);
  }

  createServices(service) {
    return this.http.post(`${environment.url + environment.endpoints.services }`, service, this.httpOptions);
  }

  updateServices(id, service) {
    return this.http.put(`${environment.url + environment.endpoints.services +  id}`, service, this.httpOptions);
  }

  allTariffsName() {
    return this.http.get(`${environment.url + environment.endpoints.tariffsName }`, this.httpOptions);
  }

  createServiceTariff(tarrifs) {
    return this.http.post(`${environment.url + environment.endpoints.tarrifs }`, tarrifs, this.httpOptions);
  }
  

}