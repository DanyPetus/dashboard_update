import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  token: string;
  httpOptions: any;

  constructor(
    private http: HttpClient,
    private cookieService: CookieService
  ) {
    this.token = this.cookieService.get('token');

    this.httpOptions = {
      headers: new HttpHeaders({
        'Authorization': this.token
      })
    };
  }

  getUsers(filters: any) {
    return this.http.post(`${environment.url + environment.endpoints.users + environment.extensions.filter }`, filters, this.httpOptions);
  }

  createUser(user: any) {
    return this.http.post(`${environment.url + environment.endpoints.users }`, user, this.httpOptions);
  }

  createDocument(users: any) {
    return this.http.post(`${environment.url + environment.endpoints.documents +  environment.extensions.users }`, users, this.httpOptions);
  }

  updateUser(uid: string, user: any) {
    return this.http.put(`${environment.url + environment.endpoints.users +  uid}`, user, this.httpOptions);
  }

}