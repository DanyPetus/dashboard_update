import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class ServicesService {

  token: string;
  httpOptions: any;

  constructor(
    private http: HttpClient,
    private cookieService: CookieService
  ) {
    this.token = this.cookieService.get('token');

    this.httpOptions = {
      headers: new HttpHeaders({
        'Authorization': this.token
      })
    };
  }

  getServicesRequest(filters: any) {
    return this.http.post(`${environment.url + environment.endpoints.services + environment.extensions.request + '/filter' }`, filters, this.httpOptions);
  }

  createDocument(services: any) {
    return this.http.post(`${environment.url + environment.endpoints.documents +  environment.extensions.services }`, services, this.httpOptions);
  }

}
