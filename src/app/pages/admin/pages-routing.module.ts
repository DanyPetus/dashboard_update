import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '',  loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule) },
  { path: 'services',  loadChildren: () => import('./services/services.module').then(m => m.ServicesdModule) },
  { path: 'technicians',  loadChildren: () => import('./technicians/technicians.module').then(m => m.TechniciansModule) },
  { path: 'users',  loadChildren: () => import('./users/users.module').then(m => m.UsersModule) },
  { path: 'maps',  loadChildren: () => import('./maps/maps.module').then(m => m.MapsModule) },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
