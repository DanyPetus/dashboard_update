import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { 
  UsersService
} from 'src/app/core/services/users/users.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  users: any;
  filters  = {roleType: [1,2, 3], status: [1,2]};

  constructor(
    private router: Router,
    private usersService: UsersService,
  ) { }

  ngOnInit(): void {
    this.getUsers();
  }

  newUser() {
    this.router.navigate(['./admin/users/new']);
  }

  generateDocument(){
    this.usersService.createDocument(this.users).subscribe((data:any) => {
      window.open('https://docs.google.com/spreadsheets/d/1aOXkZB_fc4kx9tbOhkPhjhee3rPmvLlIVzMcCh0538Y/edit#gid=1110639774');
    }, (err) => {
      console.log(err);
    });
  }

  getUsers(){
    this.usersService.getUsers(this.filters).subscribe((data:any) => {
      this.users = data;
    }, (err) => {
      console.log(err);
    });
  }

  editUser(userInfo){
    this.router.navigate(['./admin/users/edit', true], {state: userInfo} );
  }

  disableUser({uid}, status){
    const update = { status };
    this.usersService.updateUser(uid, update).subscribe((data: any) => {
        this.getUsers();
    },(err) => {
        console.error(err);
    });
  }

}
