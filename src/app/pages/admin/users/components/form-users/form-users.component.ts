import { Component, OnInit, ɵConsole } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

// Libs
import Swal from 'sweetalert2'

// Services
import { UsersService } from 'src/app/core/services/users/users.service';
import { UtilsService } from 'src/app/core/services/utils/utils.service';

@Component({
  selector: 'app-form-users',
  templateUrl: './form-users.component.html',
  styleUrls: ['./form-users.component.scss']
})
export class FormUsersComponent implements OnInit {

  // Forms
  userForm: FormGroup;
  saveButton: boolean = false;
  gender: any;
  changeGender = false;
  role: any;
  changeRole = false;
  
  // Edit
  editForm = false;
  userEdit: any;
  idUser;

  // Form
  genders = [
    {
      id: 0,
      name: "Masculino",
      genderType: 1,
    },
    {
      id: 1,
      name: "Femenino",
      genderType: 2,
    },
    {
      id: 2,
      name: "Sin Especificar",
      genderType: 3,
    },
  ];

  roles = [
    {
      id: 0,
      name: "Admin",
      roleType: 1,
    },
    {
      id: 1,
      name: "Sub Administrador",
      roleType: 2,
    },
    {
      id: 2,
      name: "Cliente",
      roleType: 3,
    },
    {
      id: 3,
      name: "Técnico",
      roleType: 4,
    },
  ];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private usersService: UsersService,
    private utilsService: UtilsService,
  ) {
    this.userEdit = this.router.getCurrentNavigation().extras.state;
    this.route.params.subscribe(params => {
      this.editForm = params['status'];
    });
  }

  ngOnInit(): void {
    this.initForm();

    if (this.userEdit && this.editForm) {
      this.getPatchValues(this.userEdit);
    } else if (this.editForm && !this.userEdit) {
      this.router.navigate(['./admin/users']);
    }
  }

  initForm() {
    this.userForm = this.fb.group({
      firstName: ['', [Validators.required]],
      secondName: ['', [Validators.required]],
      email: ['', [Validators.required]],
      phone: [0, [Validators.required]],
      gender: ['', [Validators.required]],
      birthdate: ['', [Validators.required]],
      role: ['', [Validators.required]],
    })
  }

  getPatchValues(user: any) {
    this.idUser = user.uid;

    const gender = this.genders.find(element => element.name === user.gender);
    const role = this.roles.find(element => element.roleType === user.roleType);

    const birthdatetimestamp = user.birthdate._seconds;
    const birthdate = new Date(birthdatetimestamp * 1000);

    this.userForm.patchValue({
      firstName: user.firstName,
      secondName: user.secondName,
      email: user.email,
      phone: user.phone,
      gender: gender.id,
      birthdate: this.utilsService.formatDate(birthdate),
      role: role.id,
    });
  }

  saveGender(event: any){
    const index = Number(event.target.value);
    this.gender = this.genders[index];
    this.changeGender = true;
  }

  saveRole(event: any){
    const index = Number(event.target.value);
    this.role = this.roles[index];
    this.changeRole = true;
  }

  saveUser() {
    if (this.userForm.valid) {
      if (!this.editForm) {
        this.handleCreateUser(this.userForm.value);
      } else {
        this.handleUpdateUser(this.userForm.value);
      }
    }
  }

  handleCreateUser(user: any) {
    const userInfo = {
      user: {
        email: user.email,
        password: "12345678"
      },
      information: {
        firstName: user.firstName,
        secondName: user.secondName,
        phone: user.phone,
        birthdate: new Date(user.birthdate),
        gender: this.gender.name,
        genderType: this.gender.genderType,
        roleName: this.role.name,
        roleType: this.role.roleType,
        imageProfile: '',
        status: 1,
        typeRegister: 1,
        createBy: "PfNvSUQkqCSWd8Tmzxe2uwfRHhy2",
        updateBy: "PfNvSUQkqCSWd8Tmzxe2uwfRHhy2"
      },
    }

    this.usersService.createUser(userInfo).subscribe((resp: any) => {
        this.successAlert('Usuario guardado', 'El usuario fue guardado con exito.');
      },
      (err) => {
        console.log(err);
      }
    );
  }

  handleUpdateUser(user) {

    let gender = 'Sin Especificar';
    if(!this.changeGender) {
      const gender = this.genders.find(element => element.name === this.userEdit.gender);
      user.gender = String(gender.id);
    }
    if(user.gender === '0') gender = 'Masculino';
    else if(user.gender === '1') gender = 'Femenino';
    else if(user.gender === '2') gender = 'Sin Especificar';

    let roleName = 'Sin especificar';
    
    if(!this.changeRole) user.role = String(this.userEdit.roleType - 1)
    if(user.role === '0') roleName = 'Admin';
    else if(user.role === '1') roleName = 'Sub Administrador';
    else if(user.role === '2') roleName = 'Cliente';
    else if(user.role === '3') roleName = 'Técnico';

    const birthdateChange = new Date(user.birthdate);
    const birthdateText = `${birthdateChange.getFullYear()}/${birthdateChange.getMonth() + 1}/${birthdateChange.getDate() + 1}`
    
    const userInfo = {
      firstName: user.firstName,
      secondName: user.secondName,
      email: user.email,
      phone: user.phone,
      gender,
      birthdate: birthdateText,
      roleName,
      roleType: Number(user.role) + 1,
    };

    this.usersService.updateUser(this.idUser, userInfo).subscribe((resp: any) => {
        this.successAlert('Usuario guardado', 'El usuario fue guardado con exito.');
      },
      (err) => {
        console.log(err);
      }
    );
  }

  successAlert(title: string, body: string) {
    Swal.fire(
      title,
      body,
      'success'
    ).then((resp => {
      this.router.navigate(['./admin/users'])
    }))
  }

}