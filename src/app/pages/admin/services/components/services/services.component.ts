import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

// Services
import { TechniciansServicesService } from 'src/app/core/services/technicians-services/technicians-services.service';

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.scss']
})
export class ServicesComponent implements OnInit {

  services = [];

  // Image
  file: any;

  constructor(
    private router: Router,
    private techniciansServicesService: TechniciansServicesService,
  ) { }

  ngOnInit(): void {
    this.getServices();
  }

  newService() {
    this.router.navigate(['./admin//services/new']);
  }

  getServices(){
    const filter = {status: [1, 2]};
    this.techniciansServicesService.getServices(filter).subscribe((data:any) => {
      this.services = data;
    }, (err) => {
      console.log(err);
    });
  }

  statusServiceUpdate(id: any, status: any) {
    const update = { status };
    this.techniciansServicesService.updateServices(id, update).subscribe((data: any) => {
        this.getServices();
    },(err) => {
        console.error(err);
    });
  }

  updateService(service){
    this.router.navigate(['./admin/services/edit', true], {state: service} );
  }

}
