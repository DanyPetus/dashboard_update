import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormTariffsComponent } from './form-tariffs.component';

describe('FormTariffsComponent', () => {
  let component: FormTariffsComponent;
  let fixture: ComponentFixture<FormTariffsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormTariffsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormTariffsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
