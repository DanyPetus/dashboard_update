import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

// Services
import {
  TechniciansServicesService
} from 'src/app/core/services/technicians-services/technicians-services.service';

@Component({
  selector: 'app-form-tariffs',
  templateUrl: './form-tariffs.component.html',
  styleUrls: ['./form-tariffs.component.scss']
})
export class FormTariffsComponent implements OnInit {

  tariffForm: FormGroup;
  tariffs;

  @Input() tariff;
  @Input() index: number;
  @Output() discard: EventEmitter < any > = new EventEmitter < any > ();

  constructor(
    private fb: FormBuilder,
    private techniciansServicesService: TechniciansServicesService,
  ) { }

  ngOnInit(): void {
    this.initForm();
    this.getTariffs();

    if(this.tariff.id) this.patchValues(this.tariff);
  }

  initForm() {
    this.tariffForm = this.fb.group({
      name: ['', [Validators.required]],
      price: [0, [Validators.required]],
    })
  }

  patchValues(tarrif: any){
    this.tariffForm.patchValue({
      name: tarrif.name,
      price: tarrif.tariff,
    });
  }

  getTariffs(){
    this.techniciansServicesService.allTariffsName().subscribe((data:any) => {
      this.tariffs = data;
    }, (err) => {
      console.log(err);
    });
  }

  saveNameTariff(event: any){
    if(!this.tariff.id){
      this.tariff.name = event.target.value;
    }else{
      this.tariff.name = Number(event.target.value);
    }
  }

  savePrice(event: any){
    if(!this.tariff.id){
      this.tariff.price = Number(event.target.value);
    }else{
      this.tariff.price = Number(event.target.value);
    }
  }

  discardFile() {
    this.discard.emit(this.index);
  }

}
