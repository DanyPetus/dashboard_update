import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';

// Libs
import { CookieService } from 'ngx-cookie-service';
import { AngularFireStorage } from '@angular/fire/storage';

// Services
import { ChatsService } from 'src/app/core/services/chats/chats.service';

@Component({
  selector: 'app-chats',
  templateUrl: './chats.component.html',
  styleUrls: ['./chats.component.scss']
})
export class ChatsComponent implements OnInit {

  // User Info
  name: string;
  uid: string;

  // Messages
  messageUid: any;
  messages: any;

  message: string;
  public messageForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private cookieService: CookieService,
    private storage: AngularFireStorage,
    private chatsService: ChatsService
  ) {
    const messageUid = this.router.getCurrentNavigation().extras.state;
    if(messageUid) this.messageUid = messageUid.uid;
  }

  ngOnInit(): void {

    if(this.messageUid){
      this.initForm();

      this.name = this.cookieService.get('name');
      this.uid = this.cookieService.get('uid');
  
      this.getChat(this.messageUid);
      this.updateChanges(this.messageUid);
    }else this.router.navigate(['./admin/services/request']);
  }

  async getChat(uid: string){
    await this.chatsService.getChat(uid).then((chat:any) => {
      const chatInfo = chat.data();
      this.messages = chatInfo.messages;
    });
  }

  async updateChanges(uid: string){
    this.chatsService.updateChanges(uid).onSnapshot((chat) => {
      const chatInfo = chat.data();
      this.messages = [];
      this.messages = chatInfo.messages;
    });
  }

  initForm() {
    this.messageForm = this.fb.group({
      message: ['', [Validators.required]],
    });
  }

  async fileChat(event:any){
    const file = event.target.files[0];
    const pathImage = await this.createImage(file);
    this.createMessageFile(pathImage);
  }

  async createImage(file: any) {
    try {
      const code = Math.random().toString(36).substring(7);
      const nameImage = `${code}-${code}`
      await this.storage.ref('/machines').child(nameImage).put(file);
      return this.storage.ref(`machines/${nameImage}`).getDownloadURL().toPromise();
    } catch (error) {
      console.log(error);
    }
  }

  messageText(event: any){
    this.message = event.target.value;
  }

  sendMessage(){
    if (this.messageForm.valid) {
      const message = this.messageForm.value;
      this.patchValues();
      this.createMessage(message);
    }
  }

  createMessage({message}){
    const messageInfo = {
      createDate: new Date(),
      message,
      name: this.name,
      uid: this.uid, 
      type: 1,
    };

    this.getChatMessageSend(messageInfo);
  }

  createMessageFile(path: string){
    const messageInfo = {
      createDate: new Date(),
      path,
      name: this.name,
      uid: this.uid, 
      type: 2,
    };

    this.getChatMessageSend(messageInfo);
  }

  async getChatMessageSend(messageInfo: any){
    await this.chatsService.getChat(this.messageUid).then((chat:any) => {
      const chatInfo = chat.data();
      chatInfo.messages.push(messageInfo);
      this.updateMessage(chatInfo);
    });
  }

  async updateMessage(chatInfo: any){
    this.chatsService.updateChat(this.messageUid, chatInfo).then(() => {
    });
  }

  patchValues(){
    this.messageForm.patchValue({
      message: '',
    });
  }

}
