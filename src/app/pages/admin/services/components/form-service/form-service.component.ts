import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

// Libs
import Swal from 'sweetalert2'
import { AngularFireStorage } from '@angular/fire/storage';


// Services
import { AuthService } from 'src/app/core/services/auth/auth.service';
import { TechniciansServicesService } from 'src/app/core/services/technicians-services/technicians-services.service';

@Component({
  selector: 'app-form-service',
  templateUrl: './form-service.component.html',
  styleUrls: ['./form-service.component.scss']
})
export class FormServiceComponent implements OnInit {

  saveButton: boolean = false;
  userInfo;

  // Edit
  serviceForm: FormGroup;
  editForm = false;
  serviceEdit: any;

  // Forms
  tariffs = [];

  // Images
  file: any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private storage: AngularFireStorage,
    private techniciansServicesService: TechniciansServicesService,
    private authService: AuthService
  ) {
    this.serviceEdit = this.router.getCurrentNavigation().extras.state;
    this.route.params.subscribe(params => {
      this.editForm = params['status'];
    });
  }

  ngOnInit() {
    this.initForm();
    this.getUserInfo();

    if (this.serviceEdit && this.editForm) {
      this.getPatchValues(this.serviceEdit);
      this.patchTarrifs(this.serviceEdit);
    } else if (this.editForm && !this.serviceEdit) {
      this.router.navigate(['./admin/services']);
    }else this.addTariff();
  }

  initForm() {
    this.serviceForm = this.fb.group({
      name: ['', [Validators.required]],
      price: [0, [Validators.required]],
      timeServices: [0, [Validators.required]],
      pathImage: [''],
      description: ['', [Validators.required]],
    })
  }

  getPatchValues(service: any) {
    this.serviceForm.patchValue({
      name: service.name,
      price: service.price,
      timeServices: service.time_services,
      description: service.description
    });
  }

  patchTarrifs({ tarrifs }) {
    tarrifs.map((tarrif: any) => {
      const tariffRestaurant = {
        id: 15,
        name: tarrif.name,
        tariff: tarrif.price,
      };

      this.tariffs.push(tariffRestaurant);
    });

    this.addTariff();
  }

  getUserInfo() {
    this.userInfo = this.authService.isLogged();
  }

  imageMachine(event: any) {
    this.file = event.target.files[0];
  }

  addTariff() {
    const tariffRestaurant = {
      name: '',
      tariff: 0,
    };

    this.tariffs.push(tariffRestaurant);
  }

  discardTariff(index: number) {
    this.tariffs.splice(index, 1);
    if (this.editForm) {
      const update = {
        status: 2
      };
    }
  }

  async createService() {
    if (this.serviceForm.valid) {
      this.saveButton = true;
      const service = this.serviceForm.value;

      let pathImage = '';
      if(this.file) pathImage = await this.createImageMachine();
      if (!this.serviceEdit) this.handleCreateService(service, pathImage)
      else this.handleUpdateService(service, pathImage);
    }
  }

  async createImageMachine() {
    try {
      const code = Math.random().toString(36).substring(7) + Math.random().toString(36).substring(7);
      const nameImage = `${code}`
      await this.storage.ref('/services').child(nameImage).put(this.file);
      return this.storage.ref(`services/${nameImage}`).getDownloadURL().toPromise();
    } catch (error) {
      console.log(error);
    }
  }

  handleCreateService(service: any, img: string) {
    const serviceInfo = {
      name: service.name,
      price: service.price,
      description: service.description,
      time_services: service.timeServices,
      img,
    }

    this.techniciansServicesService.createServices(serviceInfo).subscribe((resp: any) => {
        if (this.tariffs.length >= 1) {
          this.handleGenerateServiceTariff(resp.id);
        } else this.successAlert('Servicio guardado', 'El servicio fue guardado con exito.');
      },
      (err) => {
        console.log(err);
      }
    );
  }

  handleGenerateServiceTariff(id_service) {
    const tarrifs = {
      id_service,
      tarrifs: this.tariffs,
    };

    this.techniciansServicesService.createServiceTariff(tarrifs).subscribe((resp: any) => {
        this.successAlert('Servicio guardado', 'El servicio fue guardado con exito.');
      },
      (err) => {
        console.log(err);
      }
    );
  }

  handleUpdateService(service: any, img: string){
    let image = this.serviceEdit.img;
    if(service.pathImage !== '') image = img;

    const serviceInfo = {
      name: service.name,
      price: service.price,
      description: service.description,
      time_services: service.timeServices,
      img: image,
    }

    this.techniciansServicesService.updateServices(this.serviceEdit.id, serviceInfo).subscribe((data: any) => {
      this.successAlert('Servicio Actualizado', 'El servicio fue actualizado con exito.');
    },(err) => {
        console.error(err);
    });
  }

  successAlert(title: string, body: string) {
    Swal.fire(
      title,
      body,
      'success'
    ).then((resp => {
      this.router.navigate(['./admin/services'])
    }))
  }

}