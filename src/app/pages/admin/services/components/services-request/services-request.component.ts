import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

// Services
import { ServicesService } from 'src/app/core/services/services/services.service';
import { ChatsService } from 'src/app/core/services/chats/chats.service';

// Libs
import Swal from 'sweetalert2'

@Component({
  selector: 'app-services-request',
  templateUrl: './services-request.component.html',
  styleUrls: ['./services-request.component.scss']
})
export class ServicesRequestComponent implements OnInit {

  services: any;
  filtersStatus = {}
  downloadFile = false;
  
  // Filters
  status = [
    {
      id: 0,
      name: 'Cancelados',
      status: { status: 0},
    },
    {
      id: 1,
      name: 'Completados',
      status: { status: 1},
    },
    {
      id: 2,
      name: 'En Proceso',
      status: { status: 2},
    },
    {
      id: 3,
      name: 'Pendientes de Asignar',
      status: { status: 3},
    },
    {
      id: 4,
      name: 'Todos  ',
      status: { },
    }
  ]

  constructor(
    private router: Router,
    private servicesService: ServicesService,
    private chatsService: ChatsService
  ) { }

  ngOnInit(): void {
    this.getServicesRequest();
  }

  filterStatus(event: any) {
    const statePosition = Number(event.target.value);
    this.filtersStatus = this.status[statePosition].status;
    this.services = [];
    this.getServicesRequest();
  }

  generateDocument(){
    const servicesId =  this.services.map((service: any) => {
      const idService = service.listService.id
      return idService;
    })

    Swal.showLoading();

    this.servicesService.createDocument(servicesId).subscribe((data:any) => {
      Swal.close();
      window.open('https://docs.google.com/spreadsheets/d/1IzjqDBjxgfsrNdFRKgUwrmA411vWHRZ7lz7cIRdVwYY');
    }, (err) => {
      Swal.close();
      this.errorAlert('Error generar reporte', 'Contacte al administrador si persiste el problema')
    });
  }

  getServicesRequest(){
    Swal.showLoading();
    this.servicesService.getServicesRequest(this.filtersStatus).subscribe((data:any) => {
      Swal.close();
      this.services = data;
      this.downloadFile = true;
    }, (err) => {
      console.log(err);
    });
  }

  async getChat(id:number){
    const uid = id.toString();
    await this.chatsService.getChat(uid).then(chat => {
      const chatInfo = chat.data();
      if(chatInfo) this.openChat(uid);
      else this.errorChat();
    });
  }

  openChat(uid: string) {
    this.router.navigate(['./admin/services/chats'], {state: { uid }} );
  }

  errorAlert(title: string, body: string) {
    Swal.fire(
      title,
      body,
      'error'
    ).then((resp => {
    }))
  }

  errorChat(){
    Swal.fire({
      title: 'No existe un registro de chat para este servicio.',
      icon: 'warning',
      confirmButtonColor: '#3085d6',
      confirmButtonText: 'Aceptar',
    }).then((result) => {

    })
  }

}
