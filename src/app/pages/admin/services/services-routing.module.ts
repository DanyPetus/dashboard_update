import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Components
import { ServicesComponent } from './components/services/services.component';
import { FormServiceComponent } from './components/form-service/form-service.component'
import { ServicesRequestComponent } from './components/services-request/services-request.component'
import { ChatsComponent } from './components/chats/chats.component';

const routes: Routes = [
  {
    path: '',
    component: ServicesComponent
  },
  {
    path: 'new',
    component: FormServiceComponent
  },
  {
    path: 'edit/:status',
    component: FormServiceComponent
  },
  {
    path: 'request',
    component: ServicesRequestComponent
  },
  {
    path: 'chats',
    component: ChatsComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ServicesRoutingModule { }
