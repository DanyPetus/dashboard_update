import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

//Route
import { ServicesRoutingModule } from './services-routing.module';

// Components
import { ServicesComponent } from './components/services/services.component';
import { FormServiceComponent } from './components/form-service/form-service.component';
import { FormTariffsComponent } from './components/form-tariffs/form-tariffs.component';
import { ChatsComponent } from './components/chats/chats.component';

// Libs
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ServicesRequestComponent } from './components/services-request/services-request.component';

@NgModule({
  declarations: [ServicesComponent, FormServiceComponent, FormTariffsComponent, ServicesRequestComponent, ChatsComponent],
  imports: [
    CommonModule,
    ServicesRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    NgxDatatableModule,
  ]
})
export class ServicesdModule { }
