import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

//Route
import { TechniciansRoutingModule } from './technicians-routing.module';

// Components
import { TechniciansComponent } from './components/technicians/technicians.component';

// Libs
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

@NgModule({
  declarations: [TechniciansComponent],
  imports: [
    CommonModule,
    TechniciansRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    NgxDatatableModule,
  ]
})
export class TechniciansModule { }
