import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Components
import { TechniciansComponent } from './components/technicians/technicians.component';

const routes: Routes = [
  {
    path: '',
    component: TechniciansComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TechniciansRoutingModule { }
