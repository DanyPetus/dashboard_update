import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Components
import { GlobalMapComponent } from './components/global-map/global-map.component';

const routes: Routes = [
  {
    path: '',
    component: GlobalMapComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MapsRoutingModule { }
