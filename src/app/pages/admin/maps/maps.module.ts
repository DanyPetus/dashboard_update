import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

//Route
import { MapsRoutingModule } from './maps-routing.module';

// Components
import { GlobalMapComponent } from './components/global-map/global-map.component';

// Libs
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { AgmCoreModule } from '@agm/core';

@NgModule({
  declarations: [GlobalMapComponent],
  imports: [
    CommonModule,
    MapsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    NgxDatatableModule,
    AgmCoreModule,
  ]
})
export class MapsModule { }
