import { Component, OnInit } from '@angular/core';

// Services
import { ServicesService } from 'src/app/core/services/services/services.service';

@Component({
  selector: 'app-global-map',
  templateUrl: './global-map.component.html',
  styleUrls: ['./global-map.component.scss']
})
export class GlobalMapComponent implements OnInit {

  latitude: Number;
  longitude: Number;
  zoom = 12;
  drawingManager: any;
  selectedShape: any;
  selectedArea = 0;
  markers = [];
  map;

  l1 = 14.553277399999999;
  l2 = -90.53539260000001;

  styles: any[] = [{
      elementType: 'geometry',
      stylers: [{
        color: '#242f3e'
      }]
    },
    {
      elementType: 'labels.text.stroke',
      stylers: [{
        color: '#242f3e'
      }]
    },
    {
      elementType: 'labels.text.fill',
      stylers: [{
        color: '#746855'
      }]
    },
    {
      featureType: 'administrative.locality',
      elementType: 'labels.text.fill',
      stylers: [{
        color: '#d59563'
      }]
    },
    {
      featureType: 'poi',
      elementType: 'labels.text.fill',
      stylers: [{
        color: '#d59563'
      }]
    },
    {
      featureType: 'poi.park',
      elementType: 'geometry',
      stylers: [{
        color: '#263c3f'
      }]
    },
    {
      featureType: 'poi.park',
      elementType: 'labels.text.fill',
      stylers: [{
        color: '#6b9a76'
      }]
    },
    {
      featureType: 'road',
      elementType: 'geometry',
      stylers: [{
        color: '#38414e'
      }]
    },
    {
      featureType: 'road',
      elementType: 'geometry.stroke',
      stylers: [{
        color: '#212a37'
      }]
    },
    {
      featureType: 'road',
      elementType: 'labels.text.fill',
      stylers: [{
        color: '#9ca5b3'
      }]
    },
    {
      featureType: 'road.highway',
      elementType: 'geometry',
      stylers: [{
        color: '#746855'
      }]
    },
    {
      featureType: 'road.highway',
      elementType: 'geometry.stroke',
      stylers: [{
        color: '#1f2835'
      }]
    },
    {
      featureType: 'road.highway',
      elementType: 'labels.text.fill',
      stylers: [{
        color: '#f3d19c'
      }]
    },
    {
      featureType: 'transit',
      elementType: 'geometry',
      stylers: [{
        color: '#2f3948'
      }]
    },
    {
      featureType: 'transit.station',
      elementType: 'labels.text.fill',
      stylers: [{
        color: '#d59563'
      }]
    },
    {
      featureType: 'water',
      elementType: 'geometry',
      stylers: [{
        color: '#17263c'
      }]
    },
    {
      featureType: 'water',
      elementType: 'labels.text.fill',
      stylers: [{
        color: '#515c6d'
      }]
    },
    {
      featureType: 'water',
      elementType: 'labels.text.stroke',
      stylers: [{
        color: '#17263c'
      }]
    }
  ]

  // Clients
  filtersStatus = {
    status: [3, 2]
  }

  constructor(
    private servicesService: ServicesService,
  ) {}

  ngOnInit(): void {
    this.setCurrentLocation();
  }

  private setCurrentLocation() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
      });
    }
  }

  onMapReady(map) {
    this.initDrawingManager(map);
  }

  initDrawingManager(map: any) {
    const options = {
      drawingControl: true,
      drawingControlOptions: {
        position: google.maps.ControlPosition.TOP_CENTER,
        drawingModes: [google.maps.drawing.OverlayType.POLYGON]
      },
      circleOptions: {
        fillColor: '#ffff00',
        fillOpacity: 1,
        strokeWeight: 5,
        clickable: false,
        editable: true,
        zIndex: 1
      }
    };
    this.drawingManager = new google.maps.drawing.DrawingManager(options);
    this.drawingManager.setMap(map);
    this.loadAllMarkers(map);
    this.markerSimulation(map);
    this.getServicesRequest();
  }


  markerSimulation(map) {
    setInterval(() => {
      this.l2 = this.l2 + 0.00010260000001;
      // this.hangeMarkerPosition(this.markers[0]);
    }, 5000);
  }

  hangeMarkerPosition(marker) {
    var latlng = new google.maps.LatLng(this.l1, this.l2);
    marker.setPosition(latlng);
  }


  loadAllMarkers(map): void {
    this.map = map;
    this.markers.push({
        position: new google.maps.LatLng(this.l1, this.l2),
        map,
        icon: '/assets/images/car.png',
        title: "Técnico"
      }),
      this.markers.push({
        position: new google.maps.LatLng(14.553277399999999, -90.42539260000001),
        map,
        icon: '/assets/images/house.png',
        title: "Técnico"
      }, {
        position: new google.maps.LatLng(14.551177113446323, -90.5458240183169),
        map,
        icon: '/assets/images/house.png',
        title: "Técnico"
      }, {
        position: new google.maps.LatLng(14.571837948055295, -90.547100749808),
        map,
        icon: '/assets/images/house.png',
        title: "Técnico"
      }, {
        position: new google.maps.LatLng(14.592272163369202, -90.51019355376307),
        map,
        icon: '/assets/images/house.png',
        title: "Técnico"
      })


    this.markers.forEach(markerInfo => {
      //Creating a new marker object
      let marker = new google.maps.Marker({});
      marker = new google.maps.Marker({
        ...markerInfo
      });

      //creating a new info window with markers info
      const infoWindow = new google.maps.InfoWindow({
        content: marker.getTitle()
      });

      //Add click event to open info window on marker
      marker.addListener("click", () => {
        infoWindow.open(marker.getMap(), marker);
      });

      //Adding marker to google map
      marker.setMap(map);
    });

  }

  getServicesRequest() {
    this.servicesService.getServicesRequest(this.filtersStatus).subscribe((services: any) => {
    }, (err) => {
      console.log(err);
    });
  }

  zoomIn() {
    this.zoom++;
  }

  zoomOut() {
    this.zoom--;
  }

}